ALTER TABLE character.skills
DROP CONSTRAINT fk_characters_skills_attribute,
ADD CONSTRAINT fk_characters_skills_attribute
  FOREIGN KEY(attribute)
    REFERENCES character.attributes(id);