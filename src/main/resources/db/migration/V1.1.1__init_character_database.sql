CREATE SCHEMA IF NOT EXISTS character;

CREATE TABLE character.attribute_names (
  id SMALLINT NOT NULL UNIQUE,
  name TEXT NOT NULL UNIQUE
);

CREATE TABLE character.skill_names (
  id SMALLINT NOT NULL UNIQUE,
  name TEXT NOT NULL UNIQUE
);

CREATE TABLE character.languages (
  id SMALLINT NOT NULL UNIQUE,
  name TEXT NOT NULL
);

CREATE TABLE character.states (
  id SMALLINT NOT NULL UNIQUE,
  name TEXT NOT NULL UNIQUE
);



CREATE TABLE character.players (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE character.characters (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  player_id INTEGER NOT NULL,

  CONSTRAINT fk_players_characters
      FOREIGN KEY(player_id)
        REFERENCES character.players(id)
);

CREATE TABLE character.statuses (
  character_id INTEGER NOT NULL UNIQUE,
  hp INTEGER NOT NULL,
  speed INTEGER NOT NULL,
  class_armor INTEGER NOT NULL,
  state_id SMALLINT,

  CONSTRAINT fk_characters_statuses
    FOREIGN KEY(character_id)
      REFERENCES character.characters(id)
);

CREATE TABLE character.character_languages (
  id SERIAL PRIMARY KEY,
  name_id SMALLINT NOT NULL,
  character_id INTEGER NOT NULL,

  CONSTRAINT fk_statuses_character_languages
    FOREIGN KEY(character_id)
      REFERENCES character.characters(id),

  CONSTRAINT fk_statuses_character_name
      FOREIGN KEY(name_id)
        REFERENCES character.languages(id)
);

CREATE TABLE character.attributes (
  character_id INTEGER NOT NULL UNIQUE,
  name SMALLINT NOT NULL,
  value INTEGER NOT NULL,
  debuff INTEGER NOT NULL,

  CONSTRAINT fk_characters_attributes_id
      FOREIGN KEY(character_id)
        REFERENCES character.characters(id)
);

CREATE TABLE character.skills (
  character_id SMALLINT NOT NULL UNIQUE,
  name_id SMALLINT NOT NULL,
  attribute SMALLINT NOT NULL,
  is_active BOOLEAN NOT NULL,

  CONSTRAINT fk_characters_skills_id
        FOREIGN KEY(character_id)
          REFERENCES character.characters(id),

  CONSTRAINT fk_characters_skills_attribute
        FOREIGN KEY(attribute)
          REFERENCES character.attributes(character_id),

  CONSTRAINT fk_characters_attributes
        FOREIGN KEY(name_id)
          REFERENCES character.skill_names(id)
);