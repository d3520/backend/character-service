CREATE TABLE character.bags (
  id INTEGER NOT NULL UNIQUE,
  item_id BIGINT NOT NULL
);

CREATE TABLE character.item_type (
  id SMALLINT NOT NULL UNIQUE,
  name TEXT NOT NULL
);

CREATE TABLE character.items (
  id BIGINT NOT NULL UNIQUE,
  name TEXT NOT NULL,
  type_id SMALLINT NOT NULL,
  bag_id INTEGER NOT NULL UNIQUE,
  description TEXT,

  CONSTRAINT fk_bags_items_id
      FOREIGN KEY(bag_id)
        REFERENCES character.bags(id),

  CONSTRAINT fk_bags_items_type
        FOREIGN KEY(type_id)
          REFERENCES character.item_type(id)
);

CREATE TABLE character.equipment (
  character_id INTEGER NOT NULL UNIQUE,
  armor_id BIGINT,
  left_arm BIGINT,
  right_arm BIGINT,
  tool BIGINT
)