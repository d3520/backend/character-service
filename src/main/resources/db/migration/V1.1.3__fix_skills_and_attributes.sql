ALTER TABLE character.attributes
DROP CONSTRAINT attributes_character_id_key CASCADE,
ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE character.skills
DROP CONSTRAINT skills_character_id_key,
ALTER COLUMN character_id TYPE INTEGER,
ADD COLUMN id SERIAL PRIMARY KEY,
ADD CONSTRAINT fk_characters_skills_attribute
  FOREIGN KEY(attribute)
    REFERENCES character.attribute_names(id);