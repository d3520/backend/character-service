package com.dnd.character.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "character", name = "attributes")
@Data
public class Attribute implements Serializable {
    private final static Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private Short name;
    @Column(name = "value")
    private Integer value;
    @Column(name = "debuff")
    private Integer debuff;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "character_id")
    private Character character;



    public Attribute setId(Integer id) {
        this.id = id;
        return this;
    }

    public Attribute setName(Short name) {
        this.name = name;
        return this;
    }

    public Attribute setValue(Integer value) {
        this.value = value;
        return this;
    }

    public Attribute setDebuff(Integer debuff) {
        this.debuff = debuff;
        return this;
    }

    public Attribute setCharacter(Character character) {
        this.character = character;
        return this;
    }
}
