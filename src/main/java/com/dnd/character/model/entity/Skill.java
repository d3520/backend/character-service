package com.dnd.character.model.entity;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "character", name = "skills")
@Getter
public class Skill implements Serializable {
    private final static Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name_id")
    private Short nameId;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attribute")
    private Attribute attribute;
    @Column(name = "is_active")
    private Boolean isActive;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "character_id")
    private Character character;

    public Skill setId(Integer id) {
        this.id = id;
        return this;
    }

    public Skill setName(Short nameId) {
        this.nameId = nameId;
        return this;
    }

    public Skill setAttribute(Attribute attribute) {
        this.attribute = attribute;
        return this;
    }

    public Skill setIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public Skill setCharacter(Character character) {
        this.character = character;
        return this;
    }
}
