package com.dnd.character.model.dto.output;

import lombok.Value;

@Value
public class AttributeOutput {
    Integer id;
    String attributeName;
    Integer value;
    Integer debuff;
    CharacterOutput characterOutput;
}
