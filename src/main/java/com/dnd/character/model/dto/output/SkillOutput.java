package com.dnd.character.model.dto.output;

import lombok.Value;

@Value
public class SkillOutput {
    Integer id;
    String name;
    String attribute;
    Integer value;
    Boolean isActive;
    Integer characterId;
}
