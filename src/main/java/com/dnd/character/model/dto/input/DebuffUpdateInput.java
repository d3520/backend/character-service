package com.dnd.character.model.dto.input;

import com.dnd.character.model.enums.AttributeEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class DebuffUpdateInput {
    AttributeEnum name;
    Integer value;

    @JsonCreator
    public DebuffUpdateInput(@JsonProperty("name") AttributeEnum name,
                             @JsonProperty("value") Integer value) {
        this.name = name;
        this.value = value;
    }
}
