package com.dnd.character.model.dto.output;

import lombok.Value;

@Value
public class PlayerOutput {
    Integer id;
    String name;
}
