package com.dnd.character.model.dto.input;

import com.dnd.character.model.enums.AttributeEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class AttributeUpdateInput {
    AttributeEnum name;
    Integer value;

    @JsonCreator
    public AttributeUpdateInput(@JsonProperty("name") AttributeEnum name,
                                @JsonProperty("value") Integer value) {
        this.name = name;
        this.value = value;
    }
}
