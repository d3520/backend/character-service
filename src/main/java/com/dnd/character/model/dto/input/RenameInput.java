package com.dnd.character.model.dto.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class RenameInput {
    Integer id;
    String name;

    @JsonCreator
    public RenameInput(@JsonProperty("id") Integer id,
                       @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }
}
