package com.dnd.character.model.dto.output;

import com.dnd.character.model.entity.Player;
import lombok.Value;

@Value
public class CharacterOutput {
    Integer id;
    String name;
    PlayerOutput player;
}
