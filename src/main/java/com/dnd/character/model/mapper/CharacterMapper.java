package com.dnd.character.model.mapper;

import com.dnd.character.model.dto.output.CharacterOutput;
import com.dnd.character.model.entity.Character;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CharacterMapper {
    private final PlayerMapper playerMapper;

    public CharacterOutput fromEntityToOutput(Character entity) {
        return new CharacterOutput(
                entity.getId(),
                entity.getName(),
                playerMapper.fromEntityToOutput(
                        entity.getPlayer()
                )
        );
    }

    public Character fromOutputToEntity(CharacterOutput output) {
        Character character = new Character();
        character.setId(output.getId());
        character.setPlayer(
                playerMapper.fromOutputToEntity(
                        output.getPlayer()
                )
        );
        character.setName(output.getName());
        return character;
    }
}
