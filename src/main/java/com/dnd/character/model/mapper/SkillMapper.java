package com.dnd.character.model.mapper;

import com.dnd.character.model.dto.output.SkillOutput;
import com.dnd.character.model.entity.Attribute;
import com.dnd.character.model.entity.Character;
import com.dnd.character.model.entity.Skill;
import com.dnd.character.model.enums.SkillEnum;
import com.dnd.character.service.AttributeService;
import com.dnd.character.service.CharacterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SkillMapper {
    private final CharacterService characterService;
    private final AttributeService attributeService;

    public SkillOutput fromEntityToOutput(Skill entity) {
        SkillEnum skillEnum = SkillEnum.getById(entity.getNameId());
        return new SkillOutput(entity.getId(),
                skillEnum.getSkill(),
                skillEnum.getAttributeEnum().getAttribute(),
                entity.getAttribute().getValue() - entity.getAttribute().getDebuff(),
                entity.getIsActive(),
                entity.getCharacter().getId());
    }

    public Skill fromOutputToEntity(SkillOutput output) {
        Character character = characterService.convert(characterService.getById(output.getId()));
        Attribute attribute = attributeService.convert(attributeService.getByCharacterIdAndName(output.getId(), output.getAttribute()));
        return new Skill()
                .setId(output.getId())
                .setName(SkillEnum.valueOf(output.getName()).getId())
                .setAttribute(attribute)
                .setIsActive(output.getIsActive())
                .setCharacter(character);
    }
}
