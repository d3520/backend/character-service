package com.dnd.character.model.mapper;

import com.dnd.character.model.dto.output.PlayerOutput;
import com.dnd.character.model.entity.Player;
import org.springframework.stereotype.Component;

@Component
public class PlayerMapper {
    public PlayerOutput fromEntityToOutput(Player entity) {
        return new PlayerOutput(
                entity.getId(),
                entity.getName());
    }

    public Player fromOutputToEntity(PlayerOutput output) {
        Player entity = new Player();
        entity.setId(output.getId());
        entity.setName(output.getName());
        return entity;
    }
}
