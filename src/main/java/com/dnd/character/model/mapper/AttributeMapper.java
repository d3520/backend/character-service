package com.dnd.character.model.mapper;

import com.dnd.character.model.dto.output.AttributeOutput;
import com.dnd.character.model.entity.Attribute;
import com.dnd.character.model.enums.AttributeEnum;
import com.dnd.character.service.CharacterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AttributeMapper {
    private final CharacterService characterService;

    public AttributeOutput fromEntityToOutput(Attribute entity) {
        return new AttributeOutput(
                entity.getId(),
                AttributeEnum.values()[entity.getName() - 1].getAttribute(),
                entity.getValue(),
                entity.getDebuff(),
                characterService.convert(entity.getCharacter()));
    }

    public Attribute fromOutputToEntity(AttributeOutput output) {
        return new Attribute()
                .setId(output.getId())
                .setName(AttributeEnum.valueOf(output.getAttributeName()).getId())
                .setValue(output.getValue())
                .setDebuff(output.getDebuff())
                .setCharacter(characterService.convert(output.getCharacterOutput()));
    }
}
