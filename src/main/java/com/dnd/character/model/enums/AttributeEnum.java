package com.dnd.character.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum AttributeEnum {
    STRANGE((short) 1, "STRANGE"),
    DEXTERITY((short) 2, "DEXTERITY"),
    CONSTITUTION((short) 3, "CONSTITUTION"),
    INTELLIGENCE((short) 4, "INTELLIGENCE"),
    WISDOM((short) 5, "WISDOM"),
    CHARISMA((short) 6, "CHARISMA");

    private final Short id;
    private final String attribute;
}
