package com.dnd.character.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum SkillEnum {
    ACROBATIC((short) 1, "Акробатика", AttributeEnum.DEXTERITY),
    ANALYZE((short) 2, "Анализ", AttributeEnum.INTELLIGENCE),
    ATHLETIC((short) 3, "Атлетика", AttributeEnum.STRANGE),
    ATTENTION((short) 4, "Внимательность", AttributeEnum.WISDOM),
    SURVIVAL((short) 5, "Выживание", AttributeEnum.WISDOM),
    PERFORMANCE((short) 6, "Выступление", AttributeEnum.CHARISMA),
    INTIMIDATION((short) 7, "Запугивание", AttributeEnum.CHARISMA),
    HISTORY((short) 8, "История", AttributeEnum.INTELLIGENCE),
    SLEIGHT_OF_HAND((short) 9, "Ловкость рук", AttributeEnum.DEXTERITY),
    ARCANE((short) 10, "Магия", AttributeEnum.INTELLIGENCE),
    MEDICINE((short) 11, "Медицина", AttributeEnum.WISDOM),
    DECEPTION((short) 12, "Обман", AttributeEnum.CHARISMA),
    NATURE((short) 13, "Природа", AttributeEnum.INTELLIGENCE),
    PENETRATION((short) 14, "Проницательность", AttributeEnum.WISDOM),
    RELIGION((short) 15, "Религия", AttributeEnum.INTELLIGENCE),
    STEALTH((short) 16, "Скрытность", AttributeEnum.DEXTERITY),
    CONVINCEMENT((short) 17, "Убеждение", AttributeEnum.CHARISMA),
    ANIMAL_CARE((short) 18, "Уход за животными", AttributeEnum.WISDOM),
    UNKNOWN((short) 19, "Неизвестный", AttributeEnum.STRANGE);

    private final Short id;
    private final String skill;
    private final AttributeEnum attributeEnum;

    public static SkillEnum getById(Short id) {
        for (SkillEnum name : values()) {
            if (name.id.equals(id)) {
                return name;
            }
        }
        return UNKNOWN;
    }
}
