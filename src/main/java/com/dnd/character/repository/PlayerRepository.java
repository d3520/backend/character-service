package com.dnd.character.repository;

import com.dnd.character.model.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
    @Modifying
    @Query("UPDATE Player p SET p.name = :name WHERE p.id = :id")
    void updatePlayerName(@Param("id") Integer id, @Param("name") String name);
}
