package com.dnd.character.repository;

import com.dnd.character.model.entity.Attribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AttributeRepository extends JpaRepository<Attribute, Integer> {
    @Modifying
    @Query("UPDATE Attribute a SET a.value = :value WHERE a.character.id = :character_id AND a.name = :name")
    void update(@Param("character_id") Integer characterId, @Param("name") Short name, @Param("value") Integer value);
    Optional<Attribute> findByCharacterIdAndName(Integer characterId, Short name);
    List<Attribute> findAllByCharacterId(Integer characterId);
    @Modifying
    @Query("UPDATE Attribute a SET a.debuff = :value WHERE a.id = :id AND a.name = :name")
    void setDebuff(Integer id, Short name, Integer value);
}
