package com.dnd.character.repository;

import com.dnd.character.model.entity.Character;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CharacterRepository extends JpaRepository<Character, Integer> {
    @Modifying
    @Query("UPDATE Character c SET c.name = :name WHERE c.id = :id")
    void updateCharacterName(@Param("id") Integer id, @Param("name") String name);

    List<Character> findAllCharacterByPlayerId(Integer id);
}
