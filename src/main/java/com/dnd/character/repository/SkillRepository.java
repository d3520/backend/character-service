package com.dnd.character.repository;

import com.dnd.character.model.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {
    Optional<Skill> findByCharacterIdAndNameId(Integer characterId, Short nameId);
    List<Skill> findByCharacterIdAndAttributeId(Integer characterId, Integer attributeId);
    List<Skill> findAllByCharacterId(Integer characterId);
}
