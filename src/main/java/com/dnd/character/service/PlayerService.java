package com.dnd.character.service;

import com.dnd.character.exception.NotFoundException;
import com.dnd.character.model.dto.output.PlayerOutput;
import com.dnd.character.model.entity.Player;
import com.dnd.character.model.mapper.PlayerMapper;
import com.dnd.character.repository.PlayerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlayerService {
    private final PlayerRepository repository;
    private final PlayerMapper mapper;

    public void createPlayer(String name) {
        if(name.isBlank()) {
            throw new IllegalArgumentException("");
        } else {
            repository.save(new Player().setName(name));
        }
    }

    @Transactional
    public void updatePlayerName(Integer id, String name) {
        repository.findById(id).ifPresentOrElse(
                v -> repository.updatePlayerName(id, name),
                () -> {
                    throw new NotFoundException("Player with id " + id + " does not exist!");
                }
        );
    }

    public Page<PlayerOutput> getAll(Pageable pageable) {
        List<PlayerOutput> list = repository.findAll(pageable)
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
        return new PageImpl<>(list, pageable, list.size());
    }

    public PlayerOutput getById(Integer id) {
        return repository.findById(id)
                .map(mapper::fromEntityToOutput)
                .orElseThrow(() -> new NotFoundException("Player with id " + id + " does not exist!"));
    }

    public void delete(Integer id) {
        repository.findById(id).ifPresentOrElse(
                v -> repository.deleteById(id),
                () -> {throw new NotFoundException("Player with id " + id + " does not exist!");}
        );
    }
}
