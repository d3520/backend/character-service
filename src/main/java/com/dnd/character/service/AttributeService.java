package com.dnd.character.service;

import com.dnd.character.exception.NotFoundException;
import com.dnd.character.model.dto.input.AttributeUpdateInput;
import com.dnd.character.model.dto.input.DebuffUpdateInput;
import com.dnd.character.model.dto.output.AttributeOutput;
import com.dnd.character.model.entity.Attribute;
import com.dnd.character.model.entity.Character;
import com.dnd.character.model.enums.AttributeEnum;
import com.dnd.character.model.mapper.AttributeMapper;
import com.dnd.character.repository.AttributeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttributeService {
    private final AttributeRepository repository;
    private final AttributeMapper mapper;

    public AttributeService(AttributeRepository repository, @Lazy AttributeMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public void createAttributes(Character character) {
        createAttribute(character, AttributeEnum.STRANGE);
        createAttribute(character, AttributeEnum.DEXTERITY);
        createAttribute(character, AttributeEnum.CONSTITUTION);
        createAttribute(character, AttributeEnum.INTELLIGENCE);
        createAttribute(character, AttributeEnum.WISDOM);
        createAttribute(character, AttributeEnum.CHARISMA);
    }

    private void createAttribute(Character character, AttributeEnum name) {
        Integer minValue = 8;
        Attribute attribute = new Attribute();
        attribute.setCharacter(character);
        attribute.setName(name.getId());
        attribute.setValue(minValue);
        attribute.setDebuff(0);
        repository.save(attribute);
    }

    @Transactional
    public void update(Integer characterId, AttributeUpdateInput input) {
        repository.update(characterId, input.getName().getId(), input.getValue());
    }

    @Transactional
    public void setDebuff(Integer id, DebuffUpdateInput input) {
        repository.setDebuff(id, input.getName().getId(), input.getValue());
    }

    public AttributeOutput getByCharacterIdAndName(Integer id, String name) {
        return repository.findByCharacterIdAndName(id, AttributeEnum.valueOf(name).getId())
                .map(mapper::fromEntityToOutput)
                .orElseThrow(() -> new NotFoundException("Character with attribute " + name + " not found"));
    }

    public List<AttributeOutput> getByCharacterId(Integer characterId) {
        return repository.findAllByCharacterId(characterId)
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
    }

    public AttributeOutput convert(Attribute entity) {
        return mapper.fromEntityToOutput(entity);
    }

    public Attribute convert(AttributeOutput output) {
        return mapper.fromOutputToEntity(output);
    }
}
