package com.dnd.character.service;

import com.dnd.character.exception.NotFoundException;
import com.dnd.character.model.dto.output.SkillOutput;
import com.dnd.character.model.entity.Attribute;
import com.dnd.character.model.entity.Character;
import com.dnd.character.model.entity.Skill;
import com.dnd.character.model.enums.SkillEnum;
import com.dnd.character.model.mapper.SkillMapper;
import com.dnd.character.repository.SkillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SkillService {
    private final SkillRepository repository;
    private final SkillMapper mapper;
    private final CharacterService characterService;
    private final AttributeService attributeService;

    public void createSkills(Integer characterId) {
        for (SkillEnum name : SkillEnum.values()) {
            if (name.equals(SkillEnum.UNKNOWN)) {
                continue;
            }
            createSkill(characterId, name);
        }
    }

    public void createSkill(Integer characterId, SkillEnum skillEnum) {
        String attributeName = skillEnum.getAttributeEnum().getAttribute();
        Attribute attribute = attributeService.convert(
                attributeService.getByCharacterIdAndName(characterId, attributeName)
        );
        Character character = characterService.convert(
                characterService.getById(characterId)
        );
        repository.save(
                new Skill()
                .setName(skillEnum.getId())
                .setAttribute(attribute)
                .setIsActive(false)
                .setCharacter(character)
        );
    }

    public SkillOutput getByCharacterIdAndName(Integer characterId, String name) {
        Skill entity = repository.findByCharacterIdAndNameId(characterId, SkillEnum.valueOf(name.toUpperCase()).getId())
                .orElseThrow(() ->
                        new NotFoundException("Skill " + name + "with character id " + characterId + "not found!"));
        return mapper.fromEntityToOutput(entity);
    }

    public List<SkillOutput> getByCharacterIdAndAttribute(Integer characterId, Integer attributeId) {
        return repository.findByCharacterIdAndAttributeId(characterId, attributeId)
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
    }

    public List<SkillOutput> getAllByCharacterId(Integer characterId) {
        return repository.findAllByCharacterId(characterId)
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
    }
}
