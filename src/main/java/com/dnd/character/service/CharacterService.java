package com.dnd.character.service;

import com.dnd.character.exception.NotFoundException;
import com.dnd.character.model.dto.output.CharacterOutput;
import com.dnd.character.model.entity.Character;
import com.dnd.character.model.mapper.CharacterMapper;
import com.dnd.character.model.mapper.PlayerMapper;
import com.dnd.character.repository.CharacterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CharacterService {
    private final CharacterRepository repository;
    private final CharacterMapper mapper;
    private final PlayerMapper playerMapper;
    private final PlayerService playerService;
    private final AttributeService attributeService;

    public void createCharacter(Integer playerId, String name) {
        if (name.isBlank()) {
            throw new IllegalArgumentException("");
        } else {
            Character character = new Character();
            character.setName(name);
            character.setPlayer(
                    playerMapper.fromOutputToEntity(
                            playerService.getById(playerId)
                    )
            );
            repository.save(character);
            attributeService.createAttributes(character);
        }
    }

    @Transactional
    public void updateCharacterName(Integer id, String name) {
        repository.findById(id).ifPresentOrElse(
                v -> repository.updateCharacterName(id, name),
                () -> {
            throw new NotFoundException("Character with id " + id + " does not exist!");
        });
    }

    public CharacterOutput getById(Integer id) {
        return mapper.fromEntityToOutput(
                repository.findById(id)
                        .orElseThrow(
                                () -> new NotFoundException("Character with id " + id + " does not exist!")
                        )
        );
    }

    public Page<CharacterOutput> getAll(Pageable pageable) {
        List<CharacterOutput> list = repository.findAll()
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
        return new PageImpl<>(list, pageable, list.size());
    }

    public Page<CharacterOutput> getAllPlayersCharacter(Integer id, Pageable pageable) {
        List<CharacterOutput> list = repository.findAllCharacterByPlayerId(id)
                .stream()
                .map(mapper::fromEntityToOutput)
                .collect(Collectors.toList());
        return new PageImpl<>(list, pageable, list.size());
    }

    public void delete(Integer id) {
        repository.findById(id).ifPresentOrElse(
                v -> repository.deleteById(id),
                () -> {throw new NotFoundException("Character with id " + id + " does not exist!");}
        );
    }

    public CharacterOutput convert(Character entity) {
        return mapper.fromEntityToOutput(entity);
    }

    public Character convert(CharacterOutput output) {
        return mapper.fromOutputToEntity(output);
    }

}
