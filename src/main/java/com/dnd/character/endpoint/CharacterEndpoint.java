package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.RenameInput;
import com.dnd.character.model.dto.output.CharacterOutput;
import com.dnd.character.service.CharacterService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/characters")
@RequiredArgsConstructor
public class CharacterEndpoint {
    private final CharacterService service;

    @PostMapping("/create")
    public void createCharacter(@RequestParam Integer playerId,
                                @RequestParam String name) {
        service.createCharacter(playerId, name);
    }

    @PutMapping("/rename")
    public void renameCharacter(@RequestBody RenameInput input) {
        service.updateCharacterName(input.getId(), input.getName());
    }

    @GetMapping("/get")
    public CharacterOutput getById(@RequestParam Integer id) {
        return service.getById(id);
    }

    @GetMapping("/get-all")
    public Page<CharacterOutput> getAllCharacters(Pageable pageable) {
        return service.getAll(pageable);
    }

    @GetMapping("/get-all-players-character")
    public Page<CharacterOutput> getAllPlayersCharacters(@RequestParam Integer id, Pageable pageable) {
        return service.getAllPlayersCharacter(id, pageable);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Integer> deleteCharacter(@PathVariable Integer id) {
        service.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
