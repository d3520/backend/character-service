package com.dnd.character.endpoint;

import com.dnd.character.model.dto.output.SkillOutput;
import com.dnd.character.service.SkillService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/characters/{characterId}/skills")
@RequiredArgsConstructor
public class SkillEndpoint {
    private final SkillService service;

    @PostMapping("/create")
    public void create(@PathVariable Integer characterId) {
        service.createSkills(characterId);
    }

    @GetMapping("/get")
    public SkillOutput get(@PathVariable Integer characterId,
                           @RequestParam String skillName) {
        return service.getByCharacterIdAndName(characterId, skillName);
    }

    @GetMapping("/get-by-attribute")
    public Iterable<SkillOutput> getByAttribute(@PathVariable Integer characterId,
                                            @RequestParam Integer attributeId) {
        return service.getByCharacterIdAndAttribute(characterId, attributeId);
    }

    @GetMapping("/get-all")
    public Iterable<SkillOutput> getAll(@PathVariable Integer characterId) {
        return service.getAllByCharacterId(characterId);
    }
}
