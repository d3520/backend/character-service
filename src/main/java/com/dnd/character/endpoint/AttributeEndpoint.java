package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.AttributeUpdateInput;
import com.dnd.character.model.dto.input.DebuffUpdateInput;
import com.dnd.character.model.dto.output.AttributeOutput;
import com.dnd.character.service.AttributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/characters/{id}/attributes")
@RequiredArgsConstructor
public class AttributeEndpoint {
    private final AttributeService attributeService;

    @GetMapping("/get")
    public AttributeOutput getAttribute(@PathVariable Integer id,
                                        @RequestParam String name) {
        return attributeService.getByCharacterIdAndName(id, name);
    }

    @GetMapping("/get-all")
    public List<AttributeOutput> getById(@PathVariable Integer id) {
        return attributeService.getByCharacterId(id);
    }

    @PutMapping("/update/value")
    public void updateAttribute(@PathVariable Integer id, @RequestBody AttributeUpdateInput input) {
        attributeService.update(id, input);
    }

    @PutMapping("/update/debuff")
    public void setDebuff(@PathVariable Integer id,
                          @RequestBody DebuffUpdateInput input) {
        attributeService.setDebuff(id, input);
    }
}
