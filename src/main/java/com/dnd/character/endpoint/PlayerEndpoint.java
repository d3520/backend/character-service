package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.RenameInput;
import com.dnd.character.model.dto.output.PlayerOutput;
import com.dnd.character.model.entity.Player;
import com.dnd.character.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/players")
@RequiredArgsConstructor
public class PlayerEndpoint {
    private final PlayerService service;

    @PostMapping("/create")
    public void createPlayer(@RequestParam String name) {
        service.createPlayer(name);
    }

    @PutMapping("/rename")
    public void renamePlayer(@RequestBody RenameInput input) {
        service.updatePlayerName(input.getId(), input.getName());
    }

    @GetMapping("/get")
    public PlayerOutput getPlayerById(@RequestParam Integer id) {
        return service.getById(id);
    }

    @GetMapping("/get-all")
    public Page<PlayerOutput> getAllPlayers(Pageable pageable) {
        return service.getAll(pageable);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Integer> deletePlayer(@PathVariable Integer id) {
        service.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
