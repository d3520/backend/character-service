package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.RenameInput;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(value = {
        "/endpoint/player_init.sql",
        "/endpoint/character_init.sql"
},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/endpoint/truncate.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class CharacterEndpointTest extends EndpointTest{
    String renameContent;

    @Test
    void shouldCreateCharacter() throws Exception{
        mockMvc.perform(post("/characters/create")
                        .param("playerId", "1")
                        .param("name", "character3")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get("/characters/get")
                        .param("id", "3")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.name").value("character3"))
                .andExpect(jsonPath("$.player.id").value(1));
    }

    @Test
    void shouldReturnCharacter() throws Exception {
        mockMvc.perform(get("/characters/get")
                        .param("id", "1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("character1"))
                .andExpect(jsonPath("$.player.id").value(1));
    }

    @Test
    void shouldReturnAllCharacters() throws Exception {
        mockMvc.perform(get("/characters/get-all")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.content.[0].id").value(1))
                .andExpect(jsonPath("$.content.[0].name").value("character1"))
                .andExpect(jsonPath("$.content.[1].id").value(2))
                .andExpect(jsonPath("$.content.[1].name").value("character2"))
                .andExpect(jsonPath("$.content.[2]").doesNotExist());
    }

    @Test
    void shouldReturnPlayerCharacters() throws Exception {
        mockMvc.perform(get("/characters/get-all-players-character")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("id", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.content.[0].id").value(1))
                .andExpect(jsonPath("$.content.[0].name").value("character1"))
                .andExpect(jsonPath("$.content[1]").doesNotExist());
    }

    @Test
    void shouldRenameCharacter() throws Exception {
        renameContent = objectMapper.writeValueAsString(
                new RenameInput(1, "character56")
        );

        mockMvc.perform(put("/characters/rename")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(renameContent))
                .andExpect(status().isOk());

        mockMvc.perform(get("/characters/get")
                        .param("id", "1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("character56"))
                .andExpect(jsonPath("$.player.id").value(1));
    }
}
