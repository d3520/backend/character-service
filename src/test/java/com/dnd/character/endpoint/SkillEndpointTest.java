package com.dnd.character.endpoint;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(value = {
        "/endpoint/player_init.sql",
        "/endpoint/character_init.sql",
        "/endpoint/attribute_init.sql",
        "/endpoint/skill_init.sql"
},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/endpoint/truncate.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SkillEndpointTest extends EndpointTest{
    @Test
    void shouldReturnSkill() throws Exception {
        mockMvc.perform(get("/characters/1/skills/get")
                .param("skillName", "ACROBATIC")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("name").value("Акробатика"))
                .andExpect(jsonPath("attribute").value("DEXTERITY"))
                .andExpect(jsonPath("value").value("8"))
                .andExpect(jsonPath("isActive").value("true"))
                .andExpect(jsonPath("characterId").value("1"));
    }

    @Test
    void shouldReturnSkillsByAttribute() throws Exception {
        mockMvc.perform(get("/characters/1/skills/get-by-attribute")
                .param("attributeId", "2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$.[0].id").value("1"))
                .andExpect(jsonPath("$.[0].name").value("Акробатика"))
                .andExpect(jsonPath("$.[0].attribute").value("DEXTERITY"))
                .andExpect(jsonPath("$.[0].value").value("8"))
                .andExpect(jsonPath("$.[0].isActive").value("true"))

                .andExpect(jsonPath("$.[1].id").value("3"))
                .andExpect(jsonPath("$.[1].name").value("Ловкость рук"))
                .andExpect(jsonPath("$.[1].attribute").value("DEXTERITY"))
                .andExpect(jsonPath("$.[1].value").value("8"))
                .andExpect(jsonPath("$.[1].isActive").value("false"));
    }

    @Test
    void shouldReturnAll() throws Exception {
        mockMvc.perform(get("/characters/1/skills/get-all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(3)))

                .andExpect(jsonPath("$.[0].id").value("1"))
                .andExpect(jsonPath("$.[0].name").value("Акробатика"))
                .andExpect(jsonPath("$.[0].attribute").value("DEXTERITY"))
                .andExpect(jsonPath("$.[0].value").value("8"))
                .andExpect(jsonPath("$.[0].isActive").value("true"))

                .andExpect(jsonPath("$.[1].id").value("2"))
                .andExpect(jsonPath("$.[1].name").value("Атлетика"))
                .andExpect(jsonPath("$.[1].attribute").value("STRANGE"))
                .andExpect(jsonPath("$.[1].value").value("8"))
                .andExpect(jsonPath("$.[1].isActive").value("false"))

                .andExpect(jsonPath("$.[2].id").value("3"))
                .andExpect(jsonPath("$.[2].name").value("Ловкость рук"))
                .andExpect(jsonPath("$.[2].attribute").value("DEXTERITY"))
                .andExpect(jsonPath("$.[2].value").value("8"))
                .andExpect(jsonPath("$.[2].isActive").value("false"));
    }

    @Test
    void shouldCreateSkill() throws Exception {
        mockMvc.perform(get("/characters/1/skills/get-all")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(3)));

        mockMvc.perform(post("/characters/1/skills/create")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get("/characters/1/skills/get-all")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(21)))

                .andExpect(jsonPath("$.[3].id").value("4"))
                .andExpect(jsonPath("$.[3].name").value("Акробатика"))
                .andExpect(jsonPath("$.[3].attribute").value("DEXTERITY"))
                .andExpect(jsonPath("$.[3].value").value("8"))
                .andExpect(jsonPath("$.[3].isActive").value("false"))

                .andExpect(jsonPath("$.[4].id").value("5"))
                .andExpect(jsonPath("$.[4].name").value("Анализ"))
                .andExpect(jsonPath("$.[4].attribute").value("INTELLIGENCE"))
                .andExpect(jsonPath("$.[4].value").value("8"))
                .andExpect(jsonPath("$.[4].isActive").value("false"));


    }
}
