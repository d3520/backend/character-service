package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.AttributeUpdateInput;
import com.dnd.character.model.dto.input.DebuffUpdateInput;
import com.dnd.character.model.enums.AttributeEnum;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(value = {
        "/endpoint/player_init.sql",
        "/endpoint/character_init.sql",
        "/endpoint/attribute_init.sql"
},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/endpoint/truncate.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class AttributeEndpointTest extends EndpointTest {
    @Test
    void shouldReturnAttribute() throws Exception {
        mockMvc.perform(get("/characters/1/attributes/get").param("name", "WISDOM")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(5))
                .andExpect(jsonPath("$.attributeName").value("WISDOM"))
                .andExpect(jsonPath("$.value").value(8))
                .andExpect(jsonPath("$.debuff").value(0))
                .andExpect(jsonPath("$.characterOutput.id").value(1));
    }

    @Test
    void shouldReturnAllAttributes() throws Exception {
        mockMvc.perform(get("/characters/1/attributes/get-all")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].attributeName").value("STRANGE"))
                .andExpect(jsonPath("$.[0].characterOutput.id").value(1))
                .andExpect(jsonPath("$.[1].attributeName").value("DEXTERITY"))
                .andExpect(jsonPath("$.[1].characterOutput.id").value(1))
                .andExpect(jsonPath("$.[2].attributeName").value("CONSTITUTION"))
                .andExpect(jsonPath("$.[2].characterOutput.id").value(1))
                .andExpect(jsonPath("$.[3].attributeName").value("INTELLIGENCE"))
                .andExpect(jsonPath("$.[3].characterOutput.id").value(1))
                .andExpect(jsonPath("$.[4].attributeName").value("WISDOM"))
                .andExpect(jsonPath("$.[4].characterOutput.id").value(1))
                .andExpect(jsonPath("$.[5].attributeName").value("CHARISMA"))
                .andExpect(jsonPath("$.[5].characterOutput.id").value(1));
    }

    @Test
    void shouldIncreaseValue() throws Exception {
        mockMvc.perform(get("/characters/1/attributes/get").param("name", "STRANGE")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.attributeName").value("STRANGE"))
                .andExpect(jsonPath("$.value").value(8))
                .andExpect(jsonPath("$.characterOutput.id").value(1));

        mockMvc.perform(put("/characters/1/attributes/update/value")
                .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(
                                new AttributeUpdateInput(AttributeEnum.STRANGE, 9)
                        )))
                .andExpect(status().isOk());

        mockMvc.perform(get("/characters/1/attributes/get").param("name", "STRANGE")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.attributeName").value("STRANGE"))
                .andExpect(jsonPath("$.value").value(9))
                .andExpect(jsonPath("$.characterOutput.id").value(1));
    }

    @Test
    void shouldSetDebuff() throws Exception {
        mockMvc.perform(get("/characters/1/attributes/get").param("name", "STRANGE")
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.attributeName").value("STRANGE"))
                .andExpect(jsonPath("$.debuff").value(0))
                .andExpect(jsonPath("$.characterOutput.id").value(1));

        mockMvc.perform(put("/characters/1/attributes/update/debuff")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(
                            new DebuffUpdateInput(AttributeEnum.STRANGE, 5)
                    )))
                .andExpect(status().isOk());

        mockMvc.perform(get("/characters/1/attributes/get").param("name", "STRANGE")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.attributeName").value("STRANGE"))
                .andExpect(jsonPath("$.debuff").value(5))
                .andExpect(jsonPath("$.characterOutput.id").value(1));
    }
}
