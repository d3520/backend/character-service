package com.dnd.character.endpoint;

import com.dnd.character.model.dto.input.RenameInput;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(value = "/endpoint/player_init.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/endpoint/truncate.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PlayerEndpointTest extends EndpointTest {

    private String renameContent;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        renameContent = objectMapper.writeValueAsString(
                new RenameInput(1, "Yuri")
        );
    }

    @Test
    void shouldRenamePlayer() throws Exception {
        mockMvc.perform(put("/players/rename")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(renameContent))
                .andExpect(status().isOk());

        mockMvc.perform(get("/players/get")
                        .param("id", "1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Yuri"));
    }

    @Test
    void shouldCreatePlayer() throws Exception {
        mockMvc.perform(post("/players/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("name", "John"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/players/get")
                        .param("id", "4")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.name").value("John"));
    }
}
