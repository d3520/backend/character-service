package com.dnd.character.service;

import com.dnd.character.exception.NotFoundException;
import com.dnd.character.model.dto.output.PlayerOutput;
import com.dnd.character.model.entity.Player;
import com.dnd.character.model.mapper.PlayerMapper;
import com.dnd.character.repository.PlayerRepository;
import com.dnd.character.service.PlayerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PlayerServiceTest {
    private PlayerService service;

    @Mock
    private PlayerRepository repository;
    @Mock
    private PlayerMapper mapper;

    private final Integer id = 1;
    private Player player;
    private PlayerOutput playerOutput;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        service = new PlayerService(repository, mapper);

        player = new Player().setName("player");
        player.setId(id);
        playerOutput = new PlayerOutput(player.getId(), player.getName());
    }

    @Test
    void shouldCreatePlayer() {
        service.createPlayer("player");

        Mockito.verify(repository, Mockito.times(1))
                .save(new Player().setName("player"));
    }

    @Test
    void shouldReturnAllPlayers() {
        List<Player> players = new ArrayList<>();
        List<PlayerOutput> expected = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Player player = new Player();
            player.setId(i);
            player.setName("name");
            players.add(player);
            expected.add(new PlayerOutput(i, "name"));
        }

        Pageable pageable = PageRequest.of(0,3, Sort.by("name"));
        Mockito.when(repository.findAll(pageable))
                .thenReturn(new PageImpl<>(players));
        for (int i = 0; i < 10; i++) {
            Mockito.when(mapper.fromEntityToOutput(players.get(i)))
                    .thenReturn(new PlayerOutput(i, "name"));
        }

        Assertions.assertEquals(expected, service.getAll(pageable).getContent());

    }

    @Test
    void shouldDeletePlayer() {
        Mockito.when(repository.findById(id))
                .thenReturn(Optional.of(player));

        service.delete(id);
        Mockito.verify(repository, Mockito.times(1))
                .deleteById(id);
    }

    @Test
    void shouldUpdatePlayerName() {
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(player));

        service.updatePlayerName(id, "newName");
        Mockito.verify(repository, Mockito.times(1))
                .updatePlayerName(id, "newName");

    }

    @Test
    void shouldFindPlayerById() {
        Mockito.when(repository.findById(id))
                .thenReturn(Optional.of(player));
        Mockito.when(mapper.fromEntityToOutput(player))
                .thenReturn(playerOutput);

        Assertions.assertEquals(playerOutput, service.getById(id));
    }

    @Test
    void shouldThrowNotFoundException() {
        Assertions.assertThrows(NotFoundException.class, () -> service.getById(3));
    }
}
