INSERT INTO character.skill_names VALUES
(1, 'ACROBATIC'), (2, 'ANALYZE'), (3, 'ATHLETIC'), (4, 'ATTENTION'),
(5, 'SURVIVAL'), (6, 'PERFOMANCE'), (7, 'INTIMIDATION'), (8, 'HISTORY'),
(9, 'SLEIGHT_OF_HAND'), (10, 'ARCANE'), (11, 'MEDICINE'), (12, 'DECEPTION'),
(13, 'NATURE'), (14, 'PENETRATION'), (15, 'RELIGION'), (16, 'STEALTH'),
(17, 'CONVINCEMENT'), (18, 'ANIMAL_CARE');

INSERT INTO character.skills (character_id, name_id, attribute, is_active)
    VALUES (1, 1, 2, true) , (1, 3, 1, false), (1, 9, 2, false);